using Examern_T1;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace PruebasPoker
{
    public class Tests
    {
       
        [Test]
        public void PruebaCantidadJugadoresMenor()
        {
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception),()=>poker.CantidadJugadores(1));
        }
        [Test]
        public void PruebaCantidadJugadoresMayor()
        {
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.CantidadJugadores(6));
        }
        [Test]
        public void PruebaCantidadJugadoresCorrecto()
        {
            var poker = new PokerGame();
            var resultado = poker.CantidadJugadores(3);
            Assert.AreEqual(true,resultado);
        }
        [Test]
        public void PruebaCartasIguales()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 1, palo = "corazones"});//carta igual
            cartas.Add(new Carta { numero = 2, palo = "treboles" });
            cartas.Add(new Carta { numero = 3, palo = "picas" });
            cartas.Add(new Carta { numero = 4, palo = "corazones" });
            cartas.Add(new Carta { numero = 5, palo = "treboles" });
            cartas.Add(new Carta { numero = 7, palo = "diamantes" });
            cartas.Add(new Carta { numero = 1, palo = "diamantes" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "corazones" });//carta igual
            cartas.Add(new Carta { numero = 10, palo = "picas" });


            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.CartasIguales(cartas));
        }
        [Test]
        public void PruebaCartasDiferentes()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 1, palo = "corazones" });
            cartas.Add(new Carta { numero = 2, palo = "treboles" });
            cartas.Add(new Carta { numero = 3, palo = "picas" });
            cartas.Add(new Carta { numero = 4, palo = "corazones" });
            cartas.Add(new Carta { numero = 5, palo = "treboles" });
            cartas.Add(new Carta { numero = 7, palo = "diamantes" });
            cartas.Add(new Carta { numero = 1, palo = "diamantes" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.CartasIguales(cartas);
            Assert.AreEqual(true, resultado);
        }
        [Test]
        public void Pruebaganarescalerareal()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 1, palo = "corazones" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 12, palo = "corazones" });
            cartas.Add(new Carta { numero = 13, palo = "corazones" });
            cartas.Add(new Carta { numero = 7, palo = "diamantes" });
            cartas.Add(new Carta { numero = 1, palo = "diamantes" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas,2);
            Assert.AreEqual("jugador 1", resultado);
        }
        [Test]
        public void Pruebaganarescalerareal2()
        {
            var cartas = new List<Carta>();
            
            cartas.Add(new Carta { numero = 7, palo = "diamantes" });
            cartas.Add(new Carta { numero = 1, palo = "diamantes" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });
            cartas.Add(new Carta { numero = 1, palo = "corazones" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 12, palo = "corazones" });
            cartas.Add(new Carta { numero = 13, palo = "corazones" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 2", resultado);
        }
        [Test]
        public void GanarRepoker()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 8, palo = "corazones" });
            cartas.Add(new Carta { numero = 8, palo = "diamantes" });
            cartas.Add(new Carta { numero = 8, palo = "picas" });
            cartas.Add(new Carta { numero = 8, palo = "treboles" });
            cartas.Add(new Carta { numero = 0, palo = "comodin" });
            cartas.Add(new Carta { numero = 9, palo = "comodin" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 1", resultado);
        }
        [Test]
        public void GanarRepoker2()
        {
            var cartas = new List<Carta>();
            
            cartas.Add(new Carta { numero = 9, palo = "comodin" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });
            cartas.Add(new Carta { numero = 8, palo = "corazones" });
            cartas.Add(new Carta { numero = 8, palo = "diamantes" });
            cartas.Add(new Carta { numero = 8, palo = "picas" });
            cartas.Add(new Carta { numero = 8, palo = "treboles" });
            cartas.Add(new Carta { numero = 0, palo = "comodin" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 2", resultado);
        }
        [Test]
        public void Ganarpoker()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 8, palo = "corazones" });
            cartas.Add(new Carta { numero = 8, palo = "diamantes" });
            cartas.Add(new Carta { numero = 8, palo = "picas" });
            cartas.Add(new Carta { numero = 8, palo = "treboles" });
            cartas.Add(new Carta { numero = 1, palo = "corazones" });
            cartas.Add(new Carta { numero = 9, palo = "comodin" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 1", resultado);
        }
        [Test]
        public void Ganarpoker2()
        {
            var cartas = new List<Carta>();
            
            cartas.Add(new Carta { numero = 9, palo = "comodin" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 11, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });
            cartas.Add(new Carta { numero = 8, palo = "corazones" });
            cartas.Add(new Carta { numero = 8, palo = "diamantes" });
            cartas.Add(new Carta { numero = 8, palo = "picas" });
            cartas.Add(new Carta { numero = 8, palo = "treboles" });
            cartas.Add(new Carta { numero = 1, palo = "corazones" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 2", resultado);
        }
        [Test]
        public void GanarFull()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 8, palo = "corazones" });
            cartas.Add(new Carta { numero = 8, palo = "diamantes" });
            cartas.Add(new Carta { numero = 8, palo = "picas" });
            cartas.Add(new Carta { numero = 9, palo = "treboles" });
            cartas.Add(new Carta { numero = 9, palo = "corazones" });
            cartas.Add(new Carta { numero = 9, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 12, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 1", resultado);
        }
        [Test]
        public void GanarFull2()
        {
            var cartas = new List<Carta>();
            
            cartas.Add(new Carta { numero = 9, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 12, palo = "picas" });
            cartas.Add(new Carta { numero = 8, palo = "corazones" });
            cartas.Add(new Carta { numero = 8, palo = "diamantes" });
            cartas.Add(new Carta { numero = 8, palo = "picas" });
            cartas.Add(new Carta { numero = 9, palo = "treboles" });
            cartas.Add(new Carta { numero = 9, palo = "corazones" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 2", resultado);
        }
        [Test]
        public void GanarColor()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 3, palo = "corazones" });
            cartas.Add(new Carta { numero = 4, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "corazones" });
            cartas.Add(new Carta { numero = 8, palo = "corazones" });
            cartas.Add(new Carta { numero = 9, palo = "corazones" });
            cartas.Add(new Carta { numero = 9, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 12, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 1", resultado);
        }
        [Test]
        public void GanarColor2()
        {
            var cartas = new List<Carta>();
            
            cartas.Add(new Carta { numero = 9, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 12, palo = "picas" });
            cartas.Add(new Carta { numero = 3, palo = "corazones" });
            cartas.Add(new Carta { numero = 4, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "corazones" });
            cartas.Add(new Carta { numero = 8, palo = "corazones" });
            cartas.Add(new Carta { numero = 9, palo = "corazones" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 1", resultado);
        }
        [Test]
        public void GanarDoblePareja()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 3, palo = "corazones" });
            cartas.Add(new Carta { numero = 3, palo = "picas" });
            cartas.Add(new Carta { numero = 1, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "treboles" });
            cartas.Add(new Carta { numero = 9, palo = "corazones" });
            cartas.Add(new Carta { numero = 9, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 12, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 2", resultado);
        }
        [Test]
        public void GanarPareja()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 3, palo = "corazones" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });
            cartas.Add(new Carta { numero = 1, palo = "corazones" });
            cartas.Add(new Carta { numero = 11, palo = "treboles" });
            cartas.Add(new Carta { numero = 9, palo = "corazones" });
            cartas.Add(new Carta { numero = 9, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 12, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("jugador 2", resultado);
        }
        [Test]
        public void empate()
        {
            var cartas = new List<Carta>();
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });
            cartas.Add(new Carta { numero = 1, palo = "treboles" });
            cartas.Add(new Carta { numero = 11, palo = "treboles" });
            cartas.Add(new Carta { numero = 9, palo = "corazones" });
            cartas.Add(new Carta { numero = 10, palo = "picas" });
            cartas.Add(new Carta { numero = 10, palo = "diamantes" });
            cartas.Add(new Carta { numero = 10, palo = "corazones" });
            cartas.Add(new Carta { numero = 1, palo = "picas" });
            cartas.Add(new Carta { numero = 12, palo = "picas" });


            var poker = new PokerGame();
            var resultado = poker.Resultado(cartas, 2);
            Assert.AreEqual("empate", resultado);
        }
    }
}