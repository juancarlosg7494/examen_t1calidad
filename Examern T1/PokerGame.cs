﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examern_T1
{
    public class PokerGame
    {
        public bool CantidadJugadores(int jugadores) {
            if (jugadores > 5 || jugadores<2) {
                throw new Exception("Cantidad de jugadores invalidos");
            }
            return true;
        }
        public bool CartasIguales(List<Carta> cartas)
        {
            for(int i =0;i<cartas.Count ;i++) {
                for (int j = 0; j < cartas.Count; j++) {
                    if (i != j) {
                        if (cartas[i].numero == cartas[j].numero && cartas[i].palo == cartas[j].palo) {
                            throw new Exception("Cartas iguales en la baraja");
                        }
                    }
                }
            }
            return true;
        }
        public String Resultado(List<Carta> cartas,int cantidadJugadores) {
            var posicion = new List<int>();
            int inicio = 0;
            for (int i = 0; i < cantidadJugadores; i++) {
                
                var auxCartas = new List<Carta>();
                for (int j = inicio ; j < (i+1) * 5; j++)
                {
                    auxCartas.Add(cartas[j]);
                }
                inicio = (i+1) * 5;
                if (EscaleraReal(auxCartas))
                {
                    posicion.Add(1);
                }
                else {
                    if (Repoker(auxCartas))
                    {
                        posicion.Add(2);
                    }
                    else {
                        if (Poker(auxCartas))
                        {
                            posicion.Add(3);
                        }
                        else {
                            if (Full(auxCartas))
                            {
                                posicion.Add(4);
                            }
                            else {
                                if (Color(auxCartas))
                                {
                                    posicion.Add(5);
                                }
                                else {
                                    if (DoblePareja(auxCartas))
                                    {
                                        posicion.Add(6);
                                    }
                                    else
                                    {
                                        if (Pareja(auxCartas))
                                        {
                                            posicion.Add(8);
                                        }
                                        else {
                                            posicion.Add(9);
                                        }
                                    }
                                }

                            }
                        }
                    }
                }

            }

            var posicionganador = 90;
            var jugadorganador = 0;
            int index = 0;
            foreach (var pos in posicion) {
                if (pos < posicionganador) {
                    posicionganador = pos;
                    jugadorganador = index;
                }
                index++;
            }
            var empate = false;
            for (int j = inicio; j < posicion.Count; j++)
            {
                if (j!= jugadorganador && posicionganador == posicion[j]) {
                    empate = true;
                };
            }
            if (empate) {

                return "empate";
            }
            jugadorganador = jugadorganador + 1;
            return "jugador " + jugadorganador;
        }

        public List<Carta> RepartirCartas(int jugadores) {
            var palos = new List<String>();
            palos.Add("corazones");
            palos.Add("treboles");
            palos.Add("picas");
            palos.Add("diamantes");

            var random = new Random();
            var cartas = new List<Carta>();
            for (int i = 0; i < jugadores; i++)
            {
                var indexPalos = random.Next(palos.Count);
                cartas.Add(new Carta { numero = random.Next(1, 14), palo = palos[indexPalos] }) ;
            }
            return cartas;
        }
        public bool EscaleraReal(List<Carta> cartas) {
            var suma = 0;
            foreach (var carta in cartas)
            {
                if (carta.palo == cartas[0].palo)
                {
                    suma += carta.numero;
                }
            }
            if (suma == 54) {
                return true;
            }
            return false;
        }


        public bool Repoker(List<Carta> cartas) {
            var iguales = 0;
            var comodin = false;
            foreach (var carta in cartas)
            {
                if (carta.numero == cartas[0].numero) {
                    iguales += 1;
                }
                if (carta.palo == "comodin") {
                    comodin = true;
                }
            }
            if (iguales == 4 && comodin == true) {
                return true;
            }
            return false;

        }

        

        public bool Poker(List<Carta> cartas)
        {
            var iguales = 0;
            var comodin = false;
            foreach (var carta in cartas)
            {
                if (carta.numero == cartas[0].numero)
                {
                    iguales += 1;
                }
                if (carta.palo == "comodin")
                {
                    comodin = true;
                }
            }
            if (iguales == 4 && comodin == false)
            {
                return true;
            }
            return false;

        }
        public bool Full(List<Carta> cartas)
        {
            var iguales1 = 0;
            var iguales2 = 0;
            var numero1 = cartas[0].numero;
            var numero2 = 0;

            foreach (var carta in cartas) {
                if (carta.numero != numero1) {
                    numero2 = carta.numero;
                }
            }
            foreach (var carta in cartas)
            {
                if (carta.numero == numero1)
                {
                    iguales1 ++;
                }
                if (carta.numero == numero2)
                {
                    iguales2++;
                }
            }
            
            if (iguales1 * iguales2 == 6)
            {
                return true;
            }
            return false;

        }

        public bool Color(List<Carta> cartas)
        {

            var suma = 0;
            foreach (var carta in cartas)
            {
                if (carta.palo == cartas[0].palo)
                {
                    suma += 1;
                }
            }
            if (suma == 5)
            {
                return true;
            }
            return false;
        }

        public bool DoblePareja(List<Carta> cartas) {
            var iguales1 = 0;
            var iguales2 = 0;
            var numero1 = cartas[0].numero;
            var numero2 = 0;

            foreach (var carta in cartas)
            {
                if (carta.numero != numero1)
                {
                    numero2 = carta.numero;
                }
            }
            foreach (var carta in cartas)
            {
                if (carta.numero == numero1)
                {
                    iguales1++;
                }
                if (carta.numero == numero2)
                {
                    iguales2++;
                }
            }

            if (iguales1 == 2  && iguales2 == 2)
            {
                return true;
            }
            return false;
        }
        public bool Pareja(List<Carta> cartas)
        {
            var igualdad = 0;
            for (int i = 0; i < cartas.Count; i++)
            {
                for (int j = 0; j < cartas.Count; j++)
                {
                    if (i != j)
                    {
                        if (cartas[i].numero == cartas[j].numero)
                        {
                            igualdad ++;
                        }
                    }
                }
            }
            
            

            if (igualdad == 2 )
            {
                return true;
            }
            return false;
        }

    }
}
